## LAB 0 - install `jq`
--------------------


## LAB 1 - play with JSON and JQ
-----------------------------

For each of the following steps, save the file and execute `cat <filename.json> | jq .` after.


1) Create a JSON file containting 1 object, that is a single field containing your favorite season and a name for this value (like possibly `"favseason"` or `"season"`). This will be a name/value pair to represent your favorite season. 
(hint: don't forget to double quote names (keys) and string vaules)
&nbsp;  
&nbsp;  
&nbsp;  



2) Edit the JSON file to include 2 more name/value pairs in the object; your age, and your name.
(hint: don't forget to separate name/value pairs with commas)  

 _PASTE SNIIPET OF YOUR JSON TO MATTERMOST. WERE PEOPLE'S JSON FORMATS DIFFERENT?_
&nbsp;  
&nbsp;  
&nbsp;  


3) Manipulate your file and ea. time run the above `cat <filename> | jq .` command.  
Ideas for manipulation:  
             > have the object on one line   
             > have each name/value pair of the object on a different line  
             > use different spacing for each line.
&nbsp;  

_DID THE jq OUTPUT LOOK THE SAME NO MATTER WHAT?_  
&nbsp;  
&nbsp;  
&nbsp;  



4) Edit the JSON file to contain 3 objects. Each object will have the same 3 fields (name/value pairs): `name`, `age` and `season`.  
   Gather information on our classmattes from Mattermost to create a JSON array:  

   Object 0 is the neighbor on your LEFT (or pick a classmatte)  
   Object 1 is yourself  
   Object 2 is the neighbor on your RIGHT (or pick a classmatte)  
  
&nbsp;  
   _NOTE: empty string vs null vaule_  
&nbsp;  
&nbsp;  

  

4) Alter that JSON file so that the 3 objects are within an array, and `cat <filename> | jq .`  again.
   
   _POST TO MM CLASSROOM. EXTRA CREDIT. HOW DO YOU USE JQ TO ONLY PRINT THE CONTENT OF THE ARRAY?_  
   
   _NOTE: jq isn't a 'linter', but notice how it can give helpful error output on malformed JSON_
   
